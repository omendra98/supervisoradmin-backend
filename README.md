# Backend API

Famstar backend REST API.

## Coding style guide and best practices

-   [Node-JS Best Practices](https://github.com/goldbergyoni/nodebestpractices#1-project-structure-practices)
-   [Google Javascript Coding Style Guide](https://google.github.io/styleguide/jsguide.html)

## Setup your clone

-   To install the npm dependencies

```
npm install
```

-   To start the REST API server

```
npm start
```
