const { validationResult } = require('express-validator');

const Worker = require('../../models/worker');

const brandMiddlewares = {};

// function to create a new user
brandMiddlewares.createWorker = (req, res) => {
    const errors = validationResult(req);
    console.log(errors);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // creating instance of user collection
        Worker.create(
            {
                email: req.body.email,
                name: req.body.name,
                mobile: req.body.mobile,
                project: req.body.project,
                address: req.body.address,
                supervisorName: req.body.supervisorName
            },
            (err, user) => {
                // check if there is any error
                if (err) return res.status(500).send({ error: true, message: ['There was a problem registering the user.'] });

                user.save((err) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    return res.status(200).send({ error: false, result: user });
                });
            }
        );
    }
};


brandMiddlewares.editWorker = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // creating new brand
        Worker.findById(
            req.params.id,
            {
                name: 1, mobile: 1, email: 1, project: 1, address: 1, supervisorName: 1
            },
            (err, brand) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                // checking which values are there
                if (req.body.name) {
                    brand.name = req.body.name;
                } // checking the name parameter
                if (req.body.mobile) {
                    brand.mobile = req.body.mobile;
                } // checking the logo parameter
                if (req.body.email) {
                    brand.email = req.body.email;
                } // checking the about parameter
                if (req.body.address) {
                    brand.address = req.body.address;
                }
                // checking the coverUrl parameter
                // updating the brand
                brand.save((err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    return res.status(200).send({ error: false, result: { worker: brand } });
                });
            }
        );
    }
};

// function to fetch all the brands of a company
brandMiddlewares.viewWorkers = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {

        Worker.find(
            {},
            { name: 1, mobile: 1, supervisorName: 1, email: 1, project: 1, address: 1 },
            (err, brand) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });
                return res.status(200).send({ error: false, result: { supers: brand } });
            }
        ).populate('project');
    }
};

// function to fetch a specific brand using brand Id
brandMiddlewares.viewWorker = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // checking if the user is the owner of the request brand

        // checking if the brand exists and returns if it exists
        if (req.params.id) {
            Worker.find(
                { project: req.params.id },
                { name: 1, mobile: 1, email: 1, supervisorName: 1, project: 1, address: 1, },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });

                    return res.status(200).send({ error: false, result: { super: brand } });
                }
            );
        } else {
            return res.status(400).send({ error: true, message: ['Not authorized to access this Project'] });
        }
    }
};

brandMiddlewares.viewWorkerById = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // checking if the user is the owner of the request brand

        // checking if the brand exists and returns if it exists
        if (req.params.id) {
            Worker.findById(
                req.params.id,
                { name: 1, mobile: 1, email: 1, supervisorName: 1, project: 1, address: 1, },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });

                    return res.status(200).send({ error: false, result: { worker: brand } });
                }
            );
        } else {
            return res.status(400).send({ error: true, message: ['Not authorized to access this Project'] });
        }
    }
};

brandMiddlewares.deleteWorker = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // checking if the user is the owner of the request brand

        // checking if the brand exists and returns if it exists
        if (req.params.id) {
            Worker.deleteOne(
                { _id: req.params.id },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (!brand) return res.status(400).send({ error: true, message: ['No such worker exists'] });

                    return res.status(200).send({ error: false, result: { worker: "Worker deleted!" } });
                }
            );
        } else {
            return res.status(400).send({ error: true, message: ['Not authorized to access this Project'] });
        }
    }
};

module.exports = brandMiddlewares;
