const { validationResult } = require('express-validator');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var crypto = require('crypto');

var config = require('../../config');

var Super = require('../../models/super');
var Admin = require('../../models/admin');

const middlewares = {};

// function to create a new user
middlewares.createUser = (req, res) => {
    const errors = validationResult(req);
    console.log(errors);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        Super.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: ['something went wrong'] });
            if (user) {
                return res.status(401).send({ error: true, message: ['User with this email already exists'] });
            }

            // hasing the password for security purposes
            var hashedPassword = bcrypt.hashSync(req.body.password);

            // creating instance of user collection
            Super.create(
                {
                    email: req.body.email,
                    password: hashedPassword,
                    name: req.body.name,
                    mobile: req.body.mobile,
                    address: req.body.address,
                },
                (err, user) => {
                    // check if there is any error
                    if (err) return res.status(500).send({ error: true, message: ['There was a problem registering the user.'] });

                    user.save((err) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        return res.status(200).send({ error: false, result: `You are successfully registered as supervisor. Please login to continue.` });
                    });
                }
            );
        });
    }
};

// function to create a new user
middlewares.createAdmin = (req, res) => {
    const errors = validationResult(req);
    console.log(errors);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        Admin.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: ['something went wrong'] });
            if (user) {
                return res.status(401).send({ error: true, message: ['User with this email already exists'] });
            }

            // hasing the password for security purposes
            var hashedPassword = bcrypt.hashSync(req.body.password);

            // creating instance of user collection
            Admin.create(
                {
                    email: req.body.email,
                    password: hashedPassword,
                    name: req.body.name,
                    mobile: req.body.mobile,
                },
                (err, user) => {
                    // check if there is any error
                    if (err) return res.status(500).send({ error: true, message: ['There was a problem registering the user.'] });

                    user.save((err) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        return res.status(200).send({ error: false, result: `You are successfully registered as Admin. Please login to continue.` });
                    });
                }
            );
        });
    }
};


// helper function to verify the user
middlewares.verifyUser = (req, res) => {
    Super.findOne({ verificationToken: req.params.token, verificationStatus: false }, (err, user) => {
        if (err) return res.status(500).send({ error: true, message: [err.message] });
        if (!user) return res.status(401).send({ error: true, message: ['Token expired or user already verified.'] });

        // updating the verification status of the user
        user.verificationStatus = true;
        user.verificationToken = undefined;
        user.save((err) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });

            // send the response
            res.status(200).render('verified');
        });
    });
};

// function to login a user
middlewares.Adminlogin = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // searching for a user instance and error handling
        Admin.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: ['Error on the server'] });
            if (!user) return res.status(404).send({ error: true, message: ['No User Found.'] });

            // comparing the password by hashing and comparing
            var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).send({ error: true, message: ['Invalid credentials'] });

            // generating token for the user
            var token = jwt.sign(
                {
                    userId: user._id,
                    userType: 'admin',
                },
                config.JWT_SECRET,
                {
                    expiresIn: 86400, //expires in 24 hours
                }
            );

            // sending the response
            res.status(200).send({ error: false, result: { auth: true, token: token, id: user._id, type: "admin" } });
        });
    }
};

middlewares.Supervisorlogin = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // searching for a user instance and error handling
        Super.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: ['Error on the server'] });
            if (!user) return res.status(404).send({ error: true, message: ['No User Found.'] });

            // comparing the password by hashing and comparing
            var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).send({ error: true, message: ['Invalid credentials'] });

            // generating token for the user
            var token = jwt.sign(
                {
                    userId: user._id,
                    userType: 'company',
                },
                config.JWT_SECRET,
                {
                    expiresIn: 86400, //expires in 24 hours
                }
            );

            // sending the response
            res.status(200).send({ error: false, result: { auth: true, token: token, id: user._id, type: "supervisor" } });
        });
    }
};

module.exports = middlewares;
