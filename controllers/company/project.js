const { validationResult } = require('express-validator');

const Admin = require('../../models/admin');
const Project = require('../../models/project');

const brandMiddlewares = {};

// function to create a new brand
brandMiddlewares.createProject = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // searching for a company using the JWT
        Admin.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not a valid user'] });

            // creating new brand
            Project.create(
                {
                    project_name: req.body.project_name,
                    project_desc: req.body.project_desc,
                    project_loc: req.body.project_loc,
                    supervisor: req.body.supervisor,
                    status: req.body.status,
                },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    brand.save();
                    return res.status(200).send({ error: false, result: { brand: brand } });
                }
            );
        });
    }
};

//function to assign supervisor
brandMiddlewares.editProject = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // searching for a company using the JWT
        // creating new brand
        Project.findById(
            req.params.id,
            {
                id: 1, project_name: 1, project_desc: 1, project_loc: 1, status: 1, supervisor: 1,
            },
            (err, brand) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                console.log(brand);
                // checking which values are there
                if (req.body.project_name) {
                    brand.project_name = req.body.project_name;
                } // checking the name parameter
                if (req.body.project_desc) {
                    brand.project_desc = req.body.project_desc;
                } // checking the logo parameter
                if (req.body.project_loc) {
                    brand.project_loc = req.body.project_loc;
                } // checking the about parameter
                if (req.body.address) {
                    brand.address = req.body.address;
                } // checking the about parameter
                if (req.body.status) {
                    brand.status = req.body.status;
                } // checking the about parameter
                if (req.body.supervisor) {
                    brand.supervisor = req.body.supervisor;
                }
                if (req.body.pointers) {
                    brand.pointers = req.body.pointers;
                }
                // checking the coverUrl parameter
                // updating the brand
                brand.save((err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    return res.status(200).send({ error: false, result: { project: brand } });
                });
            }
        );
    }
};


// function to fetch all the brands of a company
brandMiddlewares.viewProjects = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {

        Project.find(
            {},
            { project_name: 1, createdAt: 1, status: 1, project_desc: 1, project_loc: 1, supervisor: 1, pointers: 1 },
            (err, brand) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });
                return res.status(200).send({ error: false, result: { projects: brand } });
            }
        ).populate('supervisor');
    }
};

// function to fetch a specific brand using brand Id
brandMiddlewares.viewProject = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // checking if the user is the owner of the request brand

        // checking if the brand exists and returns if it exists
        if (req.params.id) {
            Project.findById(
                req.params.id,
                { supervisor: 1, status: 1, createdAt: 1, project_name: 1, project_desc: 1, project_loc: 1, pointers: 1 },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });

                    return res.status(200).send({ error: false, result: { brand: brand } });
                }
            ).populate('supervisor');
        } else {
            return res.status(400).send({ error: true, message: ['Not authorized to access this Project'] });
        }
    }
};

brandMiddlewares.deleteProject = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // checking if the user is the owner of the request brand

        // checking if the brand exists and returns if it exists
        if (req.params.id) {
            Project.deleteOne(
                { _id: req.params.id },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });

                    return res.status(200).send({ error: false, result: { brand: "Project Deleted Successfully!" } });
                }
            ).populate('supervisor');
        } else {
            return res.status(400).send({ error: true, message: ['Not authorized to access this Project'] });
        }
    }
};


module.exports = brandMiddlewares;
