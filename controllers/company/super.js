const { validationResult } = require('express-validator');

const Super = require('../../models/super');

const Admin = require('../../models/admin');

const brandMiddlewares = {};

//function to assign supervisor
brandMiddlewares.editSuper = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // searching for a company using the JWT
        Admin.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not a valid user'] });

            // creating new brand
            Super.findById(
                req.params.id,
                {
                    name: 1, mobile: 1, email: 1, project: 1, address: 1,
                },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    // checking which values are there
                    if (req.body.name) {
                        brand.name = req.body.name;
                    } // checking the name parameter
                    if (req.body.mobile) {
                        brand.mobile = req.body.mobile;
                    } // checking the logo parameter
                    if (req.body.email) {
                        brand.email = req.body.email;
                    } // checking the about parameter
                    if (req.body.project) {
                        brand.project = req.body.project;
                    } // checking the about parameter
                    if (req.body.address) {
                        brand.address = req.body.address;
                    }
                    // checking the coverUrl parameter
                    // updating the brand
                    brand.save((err, brand) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        return res.status(200).send({ error: false, result: { super: brand } });
                    });
                }
            );

        });
    }
};



// function to fetch all the brands of a company
brandMiddlewares.viewSupers = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {

        Super.find(
            {},
            { name: 1, mobile: 1, email: 1, project: 1, address: 1 },
            (err, brand) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });
                return res.status(200).send({ error: false, result: { supers: brand } });
            }
        );
    }
};

// function to fetch a specific brand using brand Id
brandMiddlewares.viewSuper = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // checking if the user is the owner of the request brand

        // checking if the brand exists and returns if it exists
        if (req.params.id) {
            Super.findById(
                req.params.id,
                { name: 1, mobile: 1, email: 1, project: 1, address: 1, },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });

                    return res.status(200).send({ error: false, result: { super: brand } });
                }
            ).populate('project');
        } else {
            return res.status(400).send({ error: true, message: ['Not authorized to access this Project'] });
        }
    }
};

// function to fetch a specific brand using brand Id
brandMiddlewares.viewAdmin = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // checking if the user is the owner of the request brand

        // checking if the brand exists and returns if it exists
        if (req.params.id) {
            Admin.findById(
                req.params.id,
                { name: 1, mobile: 1, email: 1, },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });

                    return res.status(200).send({ error: false, result: { admin: brand } });
                }
            );
        } else {
            return res.status(400).send({ error: true, message: ['Not authorized to access this Project'] });
        }
    }
};


module.exports = brandMiddlewares;
