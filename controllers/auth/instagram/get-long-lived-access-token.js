const axios = require('axios');

const config = require('../../../config');

const getLongLivedAccessToken = (shortLivedAccessToken) => {
    const url = `https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret=${config.APP_SECRET}&access_token=${shortLivedAccessToken}`;

    return axios
        .get(url)
        .then((response) => {
            const currentDate = new Date();
            currentDate.setSeconds(currentDate.getSeconds() + parseInt(response.data.expires_in));
            return { accessToken: response.data.access_token, expiry: currentDate };
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = getLongLivedAccessToken;
