const mongoose = require('mongoose');

const Influencer = require('../../../models/influencer');
const config = require('../../../config');

const { getProfile, assignProfile } = require('./get-profile');

mongoose
    .connect(config.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(async () => {
        console.log('Connected to MongoDB instance');
        await Influencer.find({})
            .then(async (influencers) => {
                for (const influencer of influencers) {
                    if (influencer.platforms.instagram != null) {
                        await getProfile(influencer.platforms.instagram.username)
                            .then(async (profile) => {
                                assignProfile(profile, influencer);
                                await influencer
                                    .save()
                                    .then((record) => {
                                        console.log('Influencer saved');
                                    })
                                    .catch((error) => {
                                        console.log(error);
                                    });
                            })
                            .catch((error) => {
                                console.log(error);
                            });
                    }
                }
            })
            .catch((error) => {
                console.log(error);
            });
        console.log("Influencer's instagram profiles populated.");
    })
    .catch((error) => {
        console.log(error);
    });
