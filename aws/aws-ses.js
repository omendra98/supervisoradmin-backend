const AWS = require('aws-sdk');

const config = require('../config');

// update settings
AWS.config.update({
    accessKeyId: config.SES_ACCESS_KEY_ID,
    secretAccessKey: config.SES_SECRET_ACCESS_KEY,
    region: config.SES_REGION,
});

// update api version
const ses = new AWS.SES({ apiVersion: '2010-12-01' });

const sendEmail = (to, subject, message, from) => {
    const params = {
        Destination: {
            ToAddresses: [to],
        },
        Message: {
            Body: {
                Html: {
                    Charset: 'UTF-8',
                    Data: message,
                },
                /* replace Html attribute with the following if you want to send plain text emails. 
                Text: {
                    Charset: "UTF-8",
                    Data: message
                }
                */
            },
            Subject: {
                Charset: 'UTF-8',
                Data: subject,
            },
        },
        ReturnPath: from ? from : config.SES_FROM_DEFAULT,
        Source: from ? from : config.SES_FROM_DEFAULT,
    };

    ses.sendEmail(params, (err, data) => {
        if (err) {
            return res.status(500).send({ err: err.message });
        } else {
            console.log('Email Sent.', data);
        }
    });
};

module.exports = { sendEmail };
