const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const config = require('./config');


const companyAuthRoutes = require('./routes/company/auth');
const projectRoutes = require('./routes/company/project');
const superRoutes = require('./routes/company/super');
const workerRoutes = require('./routes/company/worker');

const app = express();

// view engine setup
app.set('views', './views');
app.set('view engine', 'pug');

// Enable CORS requests
app.use(cors());

// Use JSON body parser
app.use(bodyParser.json());

app.use(express.static('public'));

// for parsing application/xwww-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Use HTTP request console logging
app.use(morgan('combined'));

// Connect to MongoDB Atlas instance
mongoose
    .connect(config.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Connected to MongoDB instance');
    })
    .catch((error) => {
        console.log(error);
    });

app.use('/company', companyAuthRoutes);
app.use('/company', projectRoutes);
app.use('/company', superRoutes);
app.use('/company', workerRoutes);

// Configure Express app
const appConfig = app.listen(config.PORT, config.HOST, () => {
    console.log(`Listening on ${appConfig.address().address}:${appConfig.address().port}`);
});
