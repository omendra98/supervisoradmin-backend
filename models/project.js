const mongoose = require('mongoose');

const ProjectSchema = new mongoose.Schema(
    {
        project_name: {
            type: String,
            default: '',
            required: [true, 'Name is required'],
        },
        project_desc: {
            type: String,
            default: '',
            required: [true, 'Desc is required'],
        },
        status: {
            type: String,
            default: '',
        },
        project_loc: {
            type: String,
            default: '',
            required: [true, 'location is required'],
        },
        supervisor: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'supervisor',
        },
        pointers: {
            brief: String,
            attendance: String,
            targetDate: Date,
            deviation: String,
            forecast: String,
            corMeasure: String,
            critical: String
        }
    },
    { timestamps: true }
);

const Project = mongoose.model('project', ProjectSchema);

module.exports = Project;
