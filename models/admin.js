const mongoose = require('mongoose');

const AdminSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: [true, 'Email is required'],
        },
        password: {
            type: String,
            required: [true, 'Password is required'],
        },
        name: {
            type: String,
            required: [true, 'Company name is required'],
        },
        mobile: {
            type: String,
            required: [true, 'Contact No. is required'],
        },
        projects: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'projects',
            },
        ],
    },
    { timestamps: true }
);

const Admin = mongoose.model('admin', AdminSchema);

module.exports = Admin;