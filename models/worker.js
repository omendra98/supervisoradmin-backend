const mongoose = require('mongoose');

const WorkerSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: [true, 'Email is required'],
        },
        name: {
            type: String,
            required: [true, 'Company name is required'],
        },
        mobile: {
            type: String,
            required: [true, 'Contact No. is required'],
        },
        address: {
            type: String
        },
        supervisorName: {
            type: String
        },
        project:
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'project',
        },
    },
    { timestamps: true }
);

const Worker = mongoose.model('worker', WorkerSchema);

module.exports = Worker;
