const mongoose = require('mongoose');

const SuperSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: [true, 'Email is required'],
        },
        password: {
            type: String,
            required: [true, 'Password is required'],
        },
        name: {
            type: String,
            required: [true, 'Company name is required'],
        },
        address: {
            type: String,
        },
        mobile: {
            type: String,
            required: [true, 'Contact No. is required'],
        },
        project:
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'project',
            null: true
        },

    },
    { timestamps: true }
);

const Super = mongoose.model('supervisor', SuperSchema);

module.exports = Super;
