const router = require('express').Router();
const { body } = require('express-validator');

const brandMiddlewares = require('../../controllers/company/project');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.post(
    '/project/create', authMiddleware('admin'), brandMiddlewares.createProject
);
router.put('/project/edit/:id', brandMiddlewares.editProject);

router.delete('/project/delete/:id', brandMiddlewares.deleteProject);

router.get('/allprojects', brandMiddlewares.viewProjects);

router.get('/project/:id', brandMiddlewares.viewProject);

module.exports = router;
