const router = require('express').Router();
const { body } = require('express-validator');

const brandMiddlewares = require('../../controllers/company/super');

const authMiddleware = require('../../controllers/auth/auth-middleware');


router.get('/allsuper', brandMiddlewares.viewSupers);

router.get('/super/:id', brandMiddlewares.viewSuper);

router.put('/super/edit/:id', authMiddleware('admin'), brandMiddlewares.editSuper);

router.get('/admin/:id', brandMiddlewares.viewAdmin);

module.exports = router;
