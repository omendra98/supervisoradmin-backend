const express = require('express');
const { body } = require('express-validator');
const router = express.Router();

const brandAuthMiddlewares = require('../../controllers/company/auth');

router.post(
    '/admin/register',
    [
        body('email').isEmail().normalizeEmail().isLength({ min: 8 }),
        body('password').trim().isLength({ min: 8 }),
        body('name').trim().isLength({ min: 4 }),
        body('mobile').trim(),
    ],
    brandAuthMiddlewares.createAdmin
);

router.post(
    '/supervisor/register',
    [
        body('email').isEmail().normalizeEmail().isLength({ min: 8 }),
        body('password').trim().isLength({ min: 8 }),
        body('name').trim().isLength({ min: 4 }),
        body('mobile').trim(),
    ],
    brandAuthMiddlewares.createUser
);

router.get('/register/verify/:token', brandAuthMiddlewares.verifyUser);

router.post('/admin/login', [body('email').isEmail().normalizeEmail(), body('password').trim().isLength({ min: 8 })], brandAuthMiddlewares.Adminlogin);

router.post('/supervisor/login', [body('email').isEmail().normalizeEmail(), body('password').trim().isLength({ min: 8 })], brandAuthMiddlewares.Supervisorlogin);

module.exports = router;
