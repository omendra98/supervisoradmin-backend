const router = require('express').Router();
const { body } = require('express-validator');

const brandMiddlewares = require('../../controllers/company/worker');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.post('/worker/create', authMiddleware('company'), brandMiddlewares.createWorker);

router.put('/worker/edit/:id', authMiddleware('company'), brandMiddlewares.editWorker);

router.get('/allworkers', brandMiddlewares.viewWorkers);

router.get('/worker/:id', brandMiddlewares.viewWorker);

router.delete('/worker/delete/:id', brandMiddlewares.deleteWorker);

router.get('/workerbyId/:id', brandMiddlewares.viewWorkerById);

module.exports = router;
